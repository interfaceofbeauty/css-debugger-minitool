/* 
 * Sketch mode function
 *
 * Hiddens all text (p, headers, etc). Does it by appending a CSS file.
 */

function sketchModeEnable() {
    var isChecked = sketchModeCheckbox.checked;
    if ( isChecked == true) {
        document.head.appendChild(sketchModeLink); // Appends dbg-sketch.css file
    }
    if ( isChecked == false ) {
        var removeLink = document.getElementById("sketch"); 
        document.head.removeChild(removeLink);
    }
}

/* 
 * Abstract mode function
 *
 * Hiddens all text (p, headers, etc). Does it by appending a CSS file.
 */

function abstractModeEnable() {
    var isChecked = abstractModeCheckbox.checked;
    if ( isChecked == true) {
        document.head.appendChild(abstractModeLink); // Appends dbg-abstract.css file
    }
    if ( isChecked == false ) {
        var removeLink = document.getElementById("abstract"); 
        document.head.removeChild(removeLink);
    }
}

/* 
 * Colorful mode function 
 *
 * Fills with different background colors the HTML elements. Does it by appending a CSS file.
 */ 

function colorfulModeEnable() {
    var isChecked = colorfulModeCheckbox.checked;
    if ( isChecked == true) {
        document.head.appendChild(colorfulModeLink); // Appends dbg-colorful.css file
    }
    if ( isChecked == false ) {
        var removeLink = document.getElementById("colorful");
        document.head.removeChild(removeLink);
    }
}

/* 
 * Grid mode function
 *
 * Gets the values provided in the input boxes and creates a new div element with them.
 */

var gridModeDiv = document.createElement("div");
gridModeDiv.setAttribute("id", "visual_grid");

function gridModeEnable() {
    var isChecked = gridModeCheckbox.checked;
    var horizontalSlots = document.getElementById('horizontalGridSlots').value; 
    var verticalSlots = document.getElementById('verticalGridSlots').value; 
    var flexWidth = "calc(" + ( 100 / horizontalSlots ) + "% - 2px)"; // Calculates dimensions
    if ( isChecked == true ) {
        document.body.insertBefore(gridModeDiv, document.body.firstChild);
        for (let i = 0; i < verticalSlots; i++) { // Counts slots
            for (let i = 0; i < horizontalSlots; i++) { 
                var gridModeDivChild = document.createElement("div"); // Creates new div for each
                gridModeDivChild.setAttribute("class", "grid_section");
                gridModeDivChild.style.minWidth = flexWidth;
                gridModeDivChild.style.maxWidth = flexWidth;
                document.getElementById('visual_grid').appendChild(gridModeDivChild); // Appends it
            }
        }
    }
    if ( isChecked == false ) {
        var thirdsList = gridModeDiv.childNodes, listLength = thirdsList.length;
        for (let i = 0; i < listLength; i++ ) {
            var gridLastChild = gridModeDiv.lastChild;
            gridModeDiv.removeChild(gridLastChild);
        }
        document.body.removeChild(grid);
    }
}

/*
 * CSS variable management functions
 *
 * These functions detect CSS variables both from the minitool and the website, 
 * letting the user manipulate them.
 */

cssDebugVariableList = [], cssNativeVariableList = [];

function cssVariableDetector() {
    var cssList = getComputedStyle(document.body), j = 0, k = 0; // Uses getComputedStyle method 
    for (let i=0; i < cssList.length; i++) {
        var itemSelected = cssList[i];
        var isDebugVar = /--dbg/g;
        if (isDebugVar.test(itemSelected)) { // Checks if style is a variable from dbg-main.css
            cssDebugVariableList[k] = itemSelected;
            k++;
        }
        else {
            if ((itemSelected[0] == "-") && (itemSelected[1] == "-")) { // Checks if style is just a variable
                cssNativeVariableList[j] = itemSelected;
                j++;
            }
        }
    }
}

function setCssVariable(event) {
    originalElement = document.getElementById(event.originalTarget.id);
    document.body.style.setProperty(originalElement.id,event.originalTarget.value); // Changes the variable value
}

function cssVariableIterator(targetList, targetDiv) { // Maybe this should be placed in html-generators
    for (let i=0; i < targetList.length; i++) {
        var currentVariable = targetList[i];
        var currentVariableStyle = getComputedStyle(document.body).getPropertyValue(currentVariable);
        inputMaker(currentVariable, "text", currentVariableStyle, targetDiv); // Creates input element for a variable
    }
}