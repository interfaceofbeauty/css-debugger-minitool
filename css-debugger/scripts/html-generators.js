/* 
 * Script caller 
 *
 * Creates the HTML elements needed to load the different .js files
 * on located the scripts/ folder.
 */

function scriptCaller(source){
    var script = document.createElement('script'); // Creates an HTML element of type script
    script.src = source;
    document.body.appendChild(script); // And appends it
}

scriptCaller('css-debugger/scripts/debug-options.js');  // Appends the functionality script
setTimeout(scriptCaller, 20, "css-debugger/scripts/menu-generation.js"); // 20 ms later appends the menu generating script

/*
 * Drag and drop 
 * 
 * These functions fix the initial position and give drag and drop functionality 
 * to the on-screen menu.
 */

function drag_start(event) {
    var style = window.getComputedStyle(event.target, null);
    event.dataTransfer.setData("text/plain",
    (parseInt(style.getPropertyValue("left"),10) - event.clientX) + ',' + (parseInt(style.getPropertyValue("top"),10) - event.clientY));
}

function drag_over(event) { 
    event.preventDefault(); 
    return false;
}

function drop(event) { 
    var offset = event.dataTransfer.getData("text/plain").split(',');
    var draggedItem = document.getElementById('nms-main-container');
    draggedItem.style.left = (event.clientX + parseInt(offset[0],10)) + 'px';
    draggedItem.style.top = (event.clientY + parseInt(offset[1],10)) + 'px';
    event.preventDefault();
    return false;
}

/*
 * Browser detection and warning
 *
 * There's a function for detecting what browser is being used and also a function that
 * generates a warning for use in case of some specific browser is detected.
 *
 * Credits for the detection function go to:
 * https://www.codegrepper.com/code-examples/javascript/javascript+detect+browser+type
 */

function detectBrowser() { 
    if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) {
        return 'Opera';
    } else if(navigator.userAgent.indexOf("Chrome") != -1 ) {
        return 'Chrome';
    } else if(navigator.userAgent.indexOf("Safari") != -1) {
        return 'Safari';
    } else if(navigator.userAgent.indexOf("Firefox") != -1 ){
        return 'Firefox';
    } else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) {
        return 'IE';
    } else {
        return 'Unknown';
    }
}

var browser = detectBrowser();

function browserWarningGenerator(browserName,bugLink){ // Maybe bugLink it's not the best name here
    var browserWarningDiv  = elementGenerator("div", null, "browser-warning-div"); // HTML elements needed are created
    var browserWarningText = elementGenerator("p", null, "browser-warning-text");
    var browserWarningInfo = elementGenerator("p", null, "browser-warning-text");
    var browserWarningLink = elementGenerator("a", null, "browser-warning-link");
    browserWarningLink.setAttribute("href", bugLink);
    textElementGenerator(browserWarningText, "This browser (" + browserName + ") doesn't support this functionality.");
    textElementGenerator(browserWarningInfo, "For more information please check:");
    textElementGenerator(browserWarningLink, bugLink);
    browserWarningDiv.appendChild(browserWarningText); // The elements are appended
    browserWarningDiv.appendChild(browserWarningInfo);
    browserWarningDiv.appendChild(browserWarningLink);
    return browserWarningDiv;
}

/*
 * HTML element generators
 *
 * This is a set of functions intended to create several different HTML elements.
 */

function elementGenerator( tag, elementId, classes ) {
    if (tag == undefined ){
        return;
    } 
        else {
            var newElement = document.createElement( tag );
        }
    if (elementId != undefined ){
        newElement.setAttribute("id", elementId );
    }
    if (classes != undefined ){
        newElement.setAttribute("class", classes );
    }
    return newElement;
}

function inputNumberGenerator ( input, min, max, value ){
    input.setAttribute("type", "number");
    input.setAttribute("min", min);
    input.setAttribute("max", max);
    input.setAttribute("value", value);
    return input;
}

function textElementGenerator(element, text){
    text = document.createTextNode(text);
    element.appendChild(text);
}

function inputCheckBoxGenerator( input, value){
    input.setAttribute("type", "checkbox");
    input.setAttribute("value", value );
}

function labelGenerator(forElement, text, classes){
    var newLabel = elementGenerator("label", null, classes);
    newLabel.setAttribute("for", forElement);
    textElementGenerator(newLabel, text);
    return newLabel;
}

function inputMaker (id, type, value, target) {
    var newInput = document.createElement("input");
    newInput.setAttribute("id", id );
    newInput.setAttribute("class", "debug-input");
    newInput.setAttribute("type", type);
    newInput.setAttribute("value", value);
    var labelForInput = document.createElement("label");
    labelForInput.setAttribute("for", id);
    labelForInput.setAttribute("class", "debug-input-label");
    labelForInput.innerHTML=id;
    var wrapper = document.createElement("div");
    wrapper.setAttribute("class","variable-content-wrapper");
    document.getElementById(target).appendChild(wrapper);
    wrapper.appendChild(labelForInput);
    wrapper.appendChild(newInput);
    newInput.addEventListener("change", setCssVariable);
}

function optionMaker (parent, value, text){
    var newOption = document.createElement("option");
    var text = document.createTextNode(text);
    newOption.setAttribute("value", value);
    newOption.appendChild(text);
    parent.appendChild(newOption);
}

function sectionGenerator(sectionId, rowText){
    var menuRow = elementGenerator("div", sectionId, "nms-menu-row");
    return menuRow;
}

function foldingButton(buttonId, text, optionalClasses ){
    var button = elementGenerator("div", buttonId, "nms-button nms-folding-element-button");
    var buttonText = elementGenerator("div", null, "nms-button-title");
    textElementGenerator(buttonText, text);
    var buttonSymbol = elementGenerator("div", null, "nms-caret");
    textElementGenerator(buttonSymbol, "▼");
    if(optionalClasses != null){
        button.classList.add(optionalClasses);
    }
    button.appendChild(buttonText);
    button.appendChild(buttonSymbol);
    return button;
}

function sectionButtonGenerator(number, text, caret) {
    var sectionButtonId = "nms-section-" + number + "-button";
    var sectionTitleId = "nms-section-" + number + "-title";
    var sectionCaretId = "nms-section-" + number + "-caret";
    var sectionButton = elementGenerator("div", sectionButtonId, "nms-section-button");
    var sectionTitle = elementGenerator("span", sectionTitleId, "nms-section-title");
    var sectionCaret = elementGenerator("span", sectionCaretId, "nms-caret");
    if(text != null){
        textElementGenerator(sectionTitle, text);
    }
    if(caret != null ){
        textElementGenerator(sectionCaret, caret);
    }
    sectionButton.appendChild(sectionTitle);
    sectionButton.appendChild(sectionCaret);
    return sectionButton
}

function sectionContent(contentID){
    var sectionContent = elementGenerator("div", contentID, "nms-section-content nms-folding-element nms-folded-element");
    return sectionContent;
}

function linkGenerator ( id, rel, href) {
    var newLink = document.createElement("link");
    newLink.setAttribute("id", id);
    newLink.setAttribute("rel", rel);
    newLink.setAttribute("href", href);
    return newLink;
}

var sketchModeLink = linkGenerator ( "sketch", "stylesheet", "css-debugger/css/dbg-sketch.css" ); // Creates links for the stylesheets
var abstractModeLink = linkGenerator ( "abstract", "stylesheet", "css-debugger/css/dbg-abstract.css" );
var colorfulModeLink = linkGenerator ( "colorful", "stylesheet", "css-debugger/css/dbg-colorful.css" );

/* 
 * Toggle function 
 *
 * This function works in the following way:
 * 1 - Receives the id of a button and the id of a target
 * 2 - Adds an event listener on the target
 * 3 - If the target contains a class indicating that it should be a foldable element
 *     then the element is assigned a CSS class that causes the folding behavior
 */

function foldingButtonGenerator(buttonId, targetId, complementClass, animationClass){
    var buttonNode = document.querySelector(buttonId);
    var buttonChildren = buttonNode.childNodes;
    var targetNodes = document.querySelectorAll(targetId);
    buttonNode.addEventListener('click', function(){ // Not a fan of this declaration method
        if ((complementClass != null) && (animationClass) != null){
            for(var h = 0; h < buttonChildren.length; h++){
                if(buttonChildren[h].classList.contains(complementClass)){
                    buttonChildren[h].classList.toggle(animationClass);
                }
            }
        }
        for (var j=0; j<targetNodes.length; j++){
            if(targetNodes[j].classList.contains("nms-folding-element")){
                targetNodes[j].classList.toggle("nms-folded-element");
            }
        }
    });
}