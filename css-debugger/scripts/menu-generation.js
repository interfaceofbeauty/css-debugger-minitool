/* 
 * Menu generation main function
 *
 * This function makes calls to the different element generators and composes
 * the structure of the menu.
 */

function menuMaker() {

    /*
     * Generate all the elements needed
     */
    
    var mainContainer            = elementGenerator("div", "nms-main-container"); // Generate the main container
    mainContainer.setAttribute("draggable", "true");

    var section0                 = sectionGenerator("nms-section0", "nms-section0"); // Generates the title row
    var section0Title            = elementGenerator("p", "nms-title", "nms-title");
    textElementGenerator(section0Title, "CSS Debugger");

    var section1                 = sectionGenerator("nms-section1", "nms-section1"); // Generates the first row
    var section1Button           = sectionButtonGenerator("1", "Debug modes", "▼");
    var section1Content          = sectionContent("nms-section-1-content");

    var sketchWrapper            = elementGenerator ("div", null, "nms-checkbox-wrapper"); // Creates Sketch Mode elements
    var sketchCBox               = elementGenerator ("input", "sketchModeCheckbox");
    var sketchLabel              = elementGenerator("span", "sketch-mode-label", "Sketch mode");
    inputCheckBoxGenerator(sketchCBox, "Sketch");
    textElementGenerator(sketchLabel, "Sketch mode");

    var abstractWrapper          = elementGenerator ("div", null, "nms-checkbox-wrapper"); // Creates Abstract Mode elements
    var abstractCBox             = elementGenerator ("input", "abstractModeCheckbox");
    var abstractLabel            = elementGenerator("span", "abstract-mode-label", "Abstract mode");
    inputCheckBoxGenerator(abstractCBox, "Abstract");
    textElementGenerator(abstractLabel, "Abstract mode");

    var colorfulWrapper          = elementGenerator ("div", null, "nms-checkbox-wrapper"); // Creates Colorful Mode elements
    var colorfulCBox             = elementGenerator ("input", "colorfulModeCheckbox");
    var colorfulLabel            = elementGenerator("span", "abstract-mode-label", "Colorful mode");
    inputCheckBoxGenerator(colorfulCBox, "Colorful");
    textElementGenerator(colorfulLabel, "Colorful mode");

    var gridWrapper              = elementGenerator ("div", null, "nms-checkbox-wrapper");  // Creates Grid Mode elements
    var gridCBox                 = elementGenerator ("input", "gridModeCheckbox");
    var gridNumWrapper           = elementGenerator ("div", "grid-title-wrapper", "");
    var gridLabel                = elementGenerator("span", "grid-mode-label", "nms-folding-element-button");
    var gridCaret                = elementGenerator("span", "gridCaret", "nms-caret nms-folding-element-button");
    var gridNumWrapper2          = elementGenerator("div", "grid-numbers-controls", "number-content-wrapper debug-wrapper nms-folding-element nms-folded-element");
    var gridHorizontal           = elementGenerator("input", "horizontalGridSlots"); 
    var gridHorizontalLabel      = labelGenerator(gridHorizontal.id, "Horizontal grid slots");
    var gridVertical             = elementGenerator("input", "verticalGridSlots");
    var gridVerticalLabel        = labelGenerator(gridVertical.id, "Vertical grid slots");
    inputNumberGenerator(gridHorizontal, 1, 100, 1 );
    inputNumberGenerator(gridVertical, 1, 100, 1 );
    textElementGenerator(gridLabel, "Grid mode");
    textElementGenerator(gridCaret, "▼" );
    inputCheckBoxGenerator(gridCBox, "Grid");

    var section2                 = sectionGenerator("section2", "section2"); // Generate menu row 2 and its button
    var section2Button           = sectionButtonGenerator("2", "Custom CSS", "▼");
    var section2Content          = sectionContent("nms-section-2-content");

    var debugVariableContainer   = elementGenerator("div", "debugger-Variable-Container", "nms-variable-container nms-folding-element "); //generate variable input list
    var debugVariableButton      = foldingButton("debug-vars-button", "Debug Variables", null); //generate debug vars button
    var debugVariableScrolly     = elementGenerator( "div", "debug-vars", "nms-variable-list nms-folding-element nms-folded-element"); //generate debug vars div
    var websiteVariableContainer = elementGenerator("div", "website-Variable-Container", "nms-variable-container nms-folding-element "); //generate variable input list
    var websiteVariableButton    = foldingButton("website-vars-button", "Website Variables", null); //generate debug vars button
    var websiteVariableScrolly   = elementGenerator( "div", "website-vars", "nms-variable-list nms-folding-element nms-folded-element"); //generate native vars scrolly div

    /*
     * Place the elements generated in order
     */

    document.body.appendChild(mainContainer); // Begins the appending of all the elements previously generated

    mainContainer.appendChild(section0); // Appends the title section
    section0.appendChild(section0Title);
    
    mainContainer.appendChild(section1); // Appends the first section
    section1.appendChild(section1Button); 
    section1.appendChild(section1Content); 
            
    section1Content.appendChild(sketchWrapper);
    sketchWrapper.appendChild(sketchCBox);
    sketchWrapper.appendChild(sketchLabel);

    section1Content.appendChild(abstractWrapper);
    abstractWrapper.appendChild(abstractCBox);
    abstractWrapper.appendChild(abstractLabel);

    section1Content.appendChild(colorfulWrapper);
    colorfulWrapper.appendChild(colorfulCBox);
    colorfulWrapper.appendChild(colorfulLabel);

    section1Content.appendChild(gridWrapper);
    gridWrapper.appendChild(gridCBox);
    gridWrapper.appendChild(gridNumWrapper);
    gridWrapper.appendChild(gridNumWrapper2);

    gridNumWrapper.appendChild(gridLabel);
    gridNumWrapper.appendChild(gridCaret);
    gridNumWrapper2.appendChild(gridHorizontal);
    gridNumWrapper2.appendChild(gridHorizontalLabel);
    gridNumWrapper2.appendChild(gridVertical);
    gridNumWrapper2.appendChild(gridVerticalLabel);

    mainContainer.appendChild(section2); // Appends the second section
    section2.appendChild(section2Button); 
    section2.appendChild(section2Content); 

    section2Content.appendChild(debugVariableContainer);
    section2Content.appendChild(websiteVariableContainer);

    debugVariableContainer.appendChild(debugVariableButton);
    debugVariableContainer.appendChild(debugVariableScrolly);
    websiteVariableContainer.appendChild(websiteVariableButton);
    websiteVariableContainer.appendChild(websiteVariableScrolly);

    /*
     * Establish folding/toggling functionality
     */

    foldingButtonGenerator("#nms-section-1-button", "#nms-section-1-content", "nms-caret", "animation-rotate-90");
    foldingButtonGenerator("#grid-title-wrapper", "#grid-numbers-controls", "nms-caret", "animation-rotate-90");
    foldingButtonGenerator("#nms-section-2-button", "#nms-section-2-content", "nms-caret", "animation-rotate-90");
    foldingButtonGenerator("#debug-vars-button", "#debug-vars", "nms-caret", "animation-rotate-90");
    foldingButtonGenerator("#website-vars-button", "#website-vars", "nms-caret", "animation-rotate-90");

    /*
     * Associate event listeners and functions to some elements
     */

    document.getElementById('gridModeCheckbox').addEventListener("change", gridModeEnable);
    document.getElementById('sketchModeCheckbox').addEventListener("change", sketchModeEnable);
    document.getElementById('abstractModeCheckbox').addEventListener("change", abstractModeEnable);
    document.getElementById('colorfulModeCheckbox').addEventListener("change", colorfulModeEnable);
    mainContainer.addEventListener('dragstart',drag_start,false);
    document.body.addEventListener('dragover',drag_over,false);
    document.body.addEventListener('drop',drop,false);

    /*
     * Check if the browser used is Chrome and send a warning message
     */

    var browser = detectBrowser();

    if (browser != "Chrome") {
        cssVariableIterator(cssDebugVariableList, "debug-vars");
        cssVariableIterator(cssNativeVariableList, "website-vars");
    } else {
        let variableBrowserAlert = browserWarningGenerator(browser, "https://bugs.chromium.org/p/chromium/issues/detail?id=949807");
        section2Content.appendChild(variableBrowserAlert);
        section2Content.removeChild(debugVariableContainer);
        section2Content.removeChild(websiteVariableContainer);
    }
}

/*
 * CSS Styling
 *
 * Create a link element with CSS styles used in the menu and append it
 */

var dbgStyles = linkGenerator("dbg-main", "stylesheet", "css-debugger/css/dbg-main.css");
document.head.appendChild(dbgStyles);

/*
 * Delay
 *
 * Establish a delay between the execution of the Custom CSS detection functions
 * and the menu generation. 
 */

setTimeout(cssVariableDetector, 50);
setTimeout(menuMaker, 100);