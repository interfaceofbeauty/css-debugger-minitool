# To do list
## CSS 
- Grid style changing feature. Through CSS variables?

- Use transparent inline styles for the gridMode div elements.

- Solve scrollbar styling issues. Firefox with *scrollbar-color* property and Chrome with *-webkit*.

- Some style changes should be taken into account such as font hierarchy, paddings, shadings and transparencies.

- Isolate draggable behaviour from text boxes.

- Use of smooth and cool animations for menu folding/folding and displaying.

- Make "debug-vars-button" and "website-vars-button" sticky on top of the scrollbar.

## JavaScript

- Implement some structures, specially those regarding decorators and frontend generation as objects.

- Solve uncaught reference for grid mode 

- Maybe dynamic path detection for script calling.

- Just an idea: New menu row with some sort of instructions on how to use the pre-made CSS classes found in dbg-main.css?

- Might be nice to rename some functions in *html-generators.js* for the sake of coherency. For example, use *sectionContentGenerator* instead of *sectionContent*. This of course might create a new problem, namely, too lenghty names, but in my opinion in this case coherency must prevail over name economy.