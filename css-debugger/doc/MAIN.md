# Documentation
This is the main documentation file.

## Folders
Tree overview.
|    |    |    |  Description  |
| -- | -- | -- | -- |
| **/css-debugger-minitool** |   |   |   |
|   | **/example-site** |  | |
|   | **/css-debugger** |   |  |
|   |   |  **/css** | Contains all the stylesheets |
|   |   |  **/doc** |  Contains the documenation |
|   |   |  **/scripts** | Contains the executable JS scripts|
## Files
Brief description of all files included.
### CSS files
- **dbg-main.css**
Contains main CSS styles.
- **dbg-abstract.css**
Contains the abstract mode necessary styles. It's called from *debug-options.js*
- **dbg-colorful.css**
Contains the colorful mode necessary styles. It's called from *debug-options.js*
- **dbg-sketch.css**
Contains the sketch mode necessary styles. It's called from *debug-options.js*

### JavaScript files
- **html-generators.js**
Creates all structures needed for menu generation, makes the script calls, and some other stuff.
- **debug-options.js**
Here are placed the functions available in the menu. If you want to add a new functionality, add it here.
- **menu-generation.js**
This file is responsible for the HTML generation (on-screen) of the draggable menu. 

## Pre-made CSS classes
There are some pre-made CSS classes available in the *dbg-main.css* file. They can be inserted by hand in the HTML elements of the web page you want to debug to provide functionality.
- **Flag class**
Highlights a desired element.
- **Deflow class**
Removes a desired element from the flow.
- **Counter class**
Acts a counter for the desired elements.
- **Diagnostic breakpoints**
Check for inconsistencies or errors at a given resolution.

## Issues
There's a known issue with Chrome browser getComputedStyles() not returning custom CSS styles (aka variables). This prevents CSS variable picker functionality from working in that browser.

https://bugs.chromium.org/p/chromium/issues/detail?id=850744

## Credits
*Add here the authors of the snippets used*