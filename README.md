# CSS Debugger MiniTool
## What's this?
A small, very simple and easy to use set of css mini-tools intended to ease the debugging of any website.

Those are tricks of the trade gathered over years of experience and packed in a simple to use, no-brainer toolkit!

## Features
- On-screen draggable menu.

- An abstract mode that hiddens all text from the site.

- A colorful mode that fills with different background colors the site elements.

- Visual grid that provides visual guidelines to help balance the design.

- A CSS variable picker, detects all custom variables on a site and lets you change them easily from the onscreen menu.

- Pre-made CSS classes contained in the *dbg-main.css* file.

## How to use
Locate where is the .HTML file you want to debug and **copy** the *css-debugger* folder in its directory. 

Then **add** this line to the .HTML file you want to debug.

    <script src="css-debugger/scripts/html-generators.js"></script>

**Reload** the web page in your browser. A draggable menu will appear in the top left corner of the screen, put it wherever you want and start using it right away. 

Finally, **check** the pre-made CSS classes in the *css-debugger/css/dbg-main.css* file with your favourite code editor.

Once you have finished just **delete** the *css-debugger* folder and the script tag in the .HTML file, and you're done. That's it! Easy, right? As easy as your future debugging!

## License
This software is distributed under the [GNU GPLv3](http://www.gnu.org/licenses/gpl-3.0.html "GNU GPLv3") license.

Copyright (c) 2021 TheWebmancers